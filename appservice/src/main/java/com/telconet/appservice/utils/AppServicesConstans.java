package com.telconet.appservice.utils;

public class AppServicesConstans {

	public static final String ERROR_INTERNO = "ALGO MALO OCURIO, LO SOLUCIONAREMOS PRONTO";
	
	public static final	String RECURSO_NO_ENCONTRADO = "EL RECURSO NO FUE ENCONTRADO";
	
	public static final	String PARAMETRO_NO_VALIDO = "EL PARAMETRO INGRESADO NO ES VALIDO";
	
	public static final	String INFO_204 = "NO HAY CONTENIDO";
	
	public static final	String FECHA_ERROR = "FORMATO DE FECHA INCORRECTO";
	
	public static final	String RECURSO_CREADO = "REGISTRO CREADO";
	
	public static final	String RECURSO_NO_CREADO = "EL REGISTRO NO PUDO SER CREADO";
	
	public static final	String RECURSO_ACTUALIZADO = "REGISTRO ACTUALIZADO";
	
	public static final String RECURSO_ELIMINADO = "REGISTRO ELIMIDADO";
	
	public static final String PROCESO_EXITOSO = "OK";
	
	
}
