package com.telconet.appservice.controller.contract;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.telconet.appservice.controller.dto.RolDTO;
import com.telconet.appservice.utils.utilitarios.RespuestaDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

public interface IRolesController {

	@Operation(method = "crearRol", operationId = "crearRol", description = "Capacidad que se encarga de crear el rol", tags = "RolEntity", summary = "crearRol")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@PostMapping(value = "/api/es/rol/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "RolType", required = true, content = @Content(schema = @Schema(implementation = RolDTO.class)))
	public ResponseEntity<Object> CrearRol(
			@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) RolDTO rolDTO);

	
	@Operation(method = "consultarRoles", operationId = "consultarRoles", description = "Capacidad que se encarga de consultar Roles", tags = "RolEntity", summary = "consultarRoles")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK", content = @Content(array = @ArraySchema(schema = @Schema(implementation = RolDTO.class))) ),
			@ApiResponse(responseCode = "204", description = "No Content", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@GetMapping(value = "/api/es/rol/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "RolType", required = true, content = @Content(schema = @Schema(implementation = RolDTO.class)))
	public ResponseEntity<Object> consultarRoles(
			@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) RolDTO rolDTO);
	
	
}
