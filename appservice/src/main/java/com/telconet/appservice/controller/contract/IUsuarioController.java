package com.telconet.appservice.controller.contract;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.telconet.appservice.controller.dto.UsuarioDTO;
import com.telconet.appservice.utils.utilitarios.RespuestaDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;


public interface IUsuarioController {

	
	@Operation(method = "crearUsuario", operationId = "crearUsuario", description = "Capacidad que se encarga de crear el usuario", tags = "UsuarioEntity", summary = "crearUsuario")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@PostMapping(value = "/api/es/usuario/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "UsuarioType", required = true, content = @Content(schema = @Schema(implementation = UsuarioDTO.class)))
	public ResponseEntity<Object> crearUsuario(
			@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) UsuarioDTO usuarioDTO);


	@Operation(method = "crearUsuario", operationId = "crearUsuario", description = "Capacidad que se encarga de modificar el usuario", tags = "UsuarioEntity", summary = "crearUsuario")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "OK", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = RespuestaDto.class))),

			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = RespuestaDto.class))) })
	@PutMapping(value = "/api/es/usuario/v1", produces = "application/json; charset=utf-8", consumes = "application/json; charset=utf-8")
	@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "UsuarioType", required = true, content = @Content(schema = @Schema(implementation = UsuarioDTO.class)))
	public ResponseEntity<Object> ActualizarUsuario(
			@Valid @org.springframework.web.bind.annotation.RequestBody(required = true) UsuarioDTO usuarioDTO);
	
	
}
